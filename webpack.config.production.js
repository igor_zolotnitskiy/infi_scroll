const webpack = require("webpack");
const path = require("path");
const TsConfigPathsPlugin = require("awesome-typescript-loader").TsConfigPathsPlugin;
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".jsx", ".css"]
    },

    entry: {
        app: path.resolve(__dirname, "src", "index.production.tsx")
    },

    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "[name].[hash].bundle.js",
        chunkFilename: "[name].[hash].chunk.js",
        publicPath: "/"
    },

    plugins: [
        new CleanWebpackPlugin([
            path.resolve(__dirname, "dist")
        ]),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        }),
        new webpack.optimize.UglifyJsPlugin(),
        new TsConfigPathsPlugin(),
        new ExtractTextPlugin("[name].[hash].styles.css"),
        new HtmlWebpackPlugin({
            title: "Тестовое задание",
            template: "index.ejs"
        })
    ],

    module: {
        rules: [
            {
                test: /\.(tsx|ts)$/,
                include: path.resolve(__dirname, "src"),
                use: ["awesome-typescript-loader"]
            },
            {
                test: /\.css$/,
                include: path.resolve(__dirname, "src"),
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            loader: "css-loader",
                            options: {
                                modules: true,
                                importLoaders: 1
                            }
                        },
                        {
                            loader: "postcss-loader",
                            options: {
                                "postcss-import": {},
                                "postcss-cssnext": {
                                    warnForDuplicates: false
                                },
                                "cssnano": {}
                            }
                        }]
                })
            }
        ]
    }
};