import {IUserScheme} from "./IUserScheme";

export interface IUsersScheme {
    /***
     * Список пользователей
     */
    result: IUserScheme[];

    /***
     * Предыдущая страница
     */
    nextPageUrl?: string;

    /***
     * Предыдущая страница
     */
    previousPageUrl?: string;
}