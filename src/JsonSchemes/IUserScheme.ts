export interface IUserScheme {
    /***
     * id пользователя
     */
    id: string;

    /***
     * Имя пользователя
     */
    name: string;

    /***
     * Ссылка на изображение пользователя
     */
    avatarUrl: string;
}