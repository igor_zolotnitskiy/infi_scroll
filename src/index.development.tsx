import "./index.css";

import * as React from "react";
import * as ReactDOM from "react-dom";
import { AppContainer } from "react-hot-loader";
import { BrowserRouter } from "react-router-dom";

// 30 млн пользователей (всегда генерируются случайные)
//import "./__apiMock__/apiMock";
import "./__apiMock__/apiMockStatic";
import { Application } from "./Components/Application";

const rootEl = document.getElementById("root");
const render = (Component: any) => {
    ReactDOM.render(
        <AppContainer>
            <BrowserRouter>
                <Component />
            </BrowserRouter>
        </AppContainer>,
        rootEl,
    );
};

render(Application);

// Hot Module Replacement API
if (module.hot) {
    module.hot.accept("./Components/Application", () => {
        const appComponent = require("./Components/Application");
        return render((appComponent as any).ApplicationComponent);
    });
}