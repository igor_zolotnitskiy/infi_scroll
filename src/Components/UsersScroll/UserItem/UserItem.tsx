import * as s from "./UserItem.css";
import * as React from "react";
import {IUserScheme} from "../../../JsonSchemes/IUserScheme";
import {Link} from "react-router-dom";
import {Avatar} from "../../Avatar/Avatar";

interface IUserItemProps extends IUserScheme {

}

export class UserItem extends React.Component<IUserItemProps, {}>{
    public static getElementById(id: string): HTMLLinkElement {
        return document.querySelector(`a[data-id="${id}"]`);
    }

    render() {
        const {id, avatarUrl, name} = this.props;
        return (
            <Link to={`/users/${id}`} className={s.user} data-id={id}>
                <Avatar containerClassName={s.avatar} src={avatarUrl} title={`Avatar: ${name}`} />
                <div className={s.name}>{name}</div>
            </Link>
        );
    }
}