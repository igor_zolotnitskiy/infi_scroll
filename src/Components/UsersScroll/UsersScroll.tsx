import * as s from "./UsersScroll.css";
import * as React from "react";
import axios from "axios";
import * as cn from "classnames";
const throttle = require("lodash/throttle");
import {IUsersScheme} from "../../JsonSchemes/IUsersScheme";
import {UserItem} from "./UserItem/UserItem";
import {Loading} from "../Loading/Loading";
import {FixedLoading} from "../Loading/FixedLoading";

interface IUsersScrollProps {
    hidden: boolean;
}

interface IPageInfo extends IUsersScheme {
    currentUrl: string;
    reactItems: React.ReactNode[];
}

interface IUsersScrollState {
    /***
     * Загрузка предыдущей страницы
     */
    isLoadingPrev: boolean;

    /***
     * Загрузка следующей страницы
     */
    isLoadingNext: boolean;

    /***
     * Обновление данных
     */
    isCommonLoading: boolean;

    /***
     * Стек страниц
     */
    pagesStack: IPageInfo[];
}

// todo(Золотницкий): Необходимо обрабатывать ситуацию, когда размер страницы меньше ViewPort
// todo(Золотницкий): Добавить проецирование текущей страницы в url, что-бы можно было добавить в закладки/передать ссылку
// todo(Золотницкий): Стоит обрабатывать ситуацию, когда прокрутка не document, а внутри элемента
// todo(Золотницкий): Проверить как работает с разными по высоте элементами
// todo(Золотницкий): Проецировать номер странице в URL, чтобы можно было скопировать и отправить ссылку
export class UsersScroll extends React.Component<IUsersScrollProps, IUsersScrollState> {
    /***
     * Размер стека
     */
    protected static stackSize = 3;

    // todo(Золотницкий): возможно, не стоит указывать в абсолютных величинах, нужно над этим подумать
    // todo(Золотницкий): протестировать все возможные крайние случаи, возможны баги на маленьких экранах
    /***
     * Пороговое значение, когда нужно инициилизировать загрузку (px)
     */
    protected static scrollThreshold = 100;

    constructor (props) {
        super(props);

        this.state = {
            isLoadingPrev: false,
            isLoadingNext: false,
            isCommonLoading: false,
            pagesStack: [],
        };
    }

    /***
     * Для прерывания запроса к api
     */
    protected readonly cancelSource = axios.CancelToken.source();

    /***
     * Значение data-id аттрибута якорного элемента к которому надо прокрутить после рендера
     */
    private tmpScrollToId: string;

    /***
     * Значение параметра top для функции el.scrollIntoView(top) на якорном элементе
     */
    private tmpScrollToTop: boolean;

    /***
     * Отступ, на величину которого нужно прокрутить после прокрутки к якорю
     */
    private tmpScrollOffset: number;

    /***
     * Предыдущее позиция прокрутки, для определения направления прокрутки
     */
    private previewScrollTop: number = 0;

    /***
     * Dom элемент
     */
    protected containerRef: HTMLDivElement;

    public componentDidMount() {
        // Регистрируются обработчики событий, resize
        window.addEventListener("scroll", this.refresh);
        window.addEventListener("resize", this.refresh);

        // Сразу загружается первая страница
        this.getPreview();
    }

    public componentWillUnmount() {
        window.removeEventListener("scroll", this.refresh);
        window.removeEventListener("resize", this.refresh);
        this.cancelSource.cancel();
    }

    public componentWillReceiveProps(nextProps: IUsersScrollProps) {
        if (this.props.hidden === true && nextProps.hidden === false) {
            setImmediate(() => {
                this.refreshStack();
                window.scrollTo(0, this.previewScrollTop);
            });
        }
    }

    public componentDidUpdate() {
        // Прокрутка к якорному элементу, если якорь задан
        if (this.tmpScrollToId) {
            const el = UserItem.getElementById(this.tmpScrollToId);
            if (el) {
                el.scrollIntoView(this.tmpScrollToTop);
                window.scrollBy(0, this.tmpScrollOffset);
            }
            this.tmpScrollToId = null;
            this.tmpScrollToTop = null;
            this.tmpScrollOffset = null;
        }
    }

    // todo(Золотницкий): Пробежаться по коду, можно ли избавиться от дублирования, и нужно ли (тупой код проще)
    /***
     * Получить следующую страницу
     */
    public getNext() {
        if (this.props.hidden) {
            return;
        }

        const pages = this.state.pagesStack;
        // url берется из последней загруженной страницы из стека
        let url: string = pages.length ? pages[pages.length - 1].nextPageUrl : "/api/users";

        // Если url не найден, значит страницы кончились
        if (url == null) {
            return;
        }

        this.setState({...this.state, isLoadingPrev: true});
        axios.get(url, {cancelToken: this.cancelSource.token})
            .then(response => this.addNextPageToStack(response.data, url))
            .catch(error => {
                if (!axios.isCancel(error)) {
                    console.error(error);
                    this.setState({...this.state, isLoadingPrev: false});
                }
            });
    }

    // todo(Золотницкий): Пробежаться по коду, можно ли избавиться от дублирования, и нужно ли (тупой код проще)
    /***
     * Получить предыдущую страницу
     */
    public getPreview() {
        if (this.props.hidden) {
            return;
        }

        const pages = this.state.pagesStack;
        // url берется из первой загруженной страницы из стека
        let url: string = pages.length ? pages[0].previousPageUrl : "/api/users";

        // Если url не найден, значит напоролись на первую страницу
        if (url == null) {
            return;
        }

        this.setState({...this.state, isLoadingNext: true});
        axios.get(url, {cancelToken: this.cancelSource.token})
            .then(response => this.addPrevPageToStack(response.data, url))
            .catch(error => {
                if (!axios.isCancel(error)) {
                    console.error(error);
                    this.setState({...this.state, isLoadingNext: false});
                }
            });
    }

    /***
     * Добавить к стеку следующую страницу
     * @param page - список пользователей
     * @param url - ссылка на страницу
     */
    protected addNextPageToStack = (page: IUsersScheme, url: string) => {
        const pages = this.state.pagesStack;
        // Сразу создаются экземпляры компонентов
        const reactItems = page.result.map(user => <UserItem key={user.id} {...user} />);
        pages.push({...page, reactItems, currentUrl: url});

        // Если стек переполнен, то удаляется первая страница и задается якорь, т.к. будет сброс позиции скролла
        if (pages.length > UsersScroll.stackSize) {
            pages.splice(0, 1);
            this.tmpScrollToId = page.result[0].id;
            this.tmpScrollToTop = false;
        }

        this.setState({...this.state, pagesStack: [...pages], isLoadingPrev: false});
    };

    /***
     * Добавить к стеку предыдущую страницу
     * @param page - список пользователей
     * @param url - ссылка на страницу
     */
    protected addPrevPageToStack = (page: IUsersScheme, url: string) => {
        const pages = this.state.pagesStack;
        const reactItems = page.result.map(user => <UserItem key={user.id} {...user} />);
        pages.unshift({...page, reactItems, currentUrl: url});

        // Если стек переполнен, то удаляется последняя страница и задается якорь, т.к. будет сброс позиции скролла
        if (pages.length > UsersScroll.stackSize) {
            pages.splice(pages.length - 1, 1);
            this.tmpScrollToId = page.result[page.result.length - 1].id;
            this.tmpScrollToTop = true;
        }

        this.setState({...this.state, pagesStack: [...pages], isLoadingNext: false});
    };

    /***
     * Определяется нужно ли запрашивать новые страницы
     */
    protected isNeedLoading = (): {next: boolean; prev: boolean} =>  {
        const scrollHeight = Math.max(
            document.body.scrollHeight, document.documentElement.scrollHeight,
            document.body.offsetHeight, document.documentElement.offsetHeight,
            document.body.clientHeight, document.documentElement.clientHeight
        );

        const innerHeight = window.innerHeight;
        const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        const scrollBottom = scrollTop + innerHeight;

        let vector: "toPrev" | "toNext";

        // Определяется направление прокрутки
        if (scrollTop > this.previewScrollTop) {
            vector = "toNext";
        } else if (scrollTop < this.previewScrollTop) {
            vector = "toPrev";
        }

        this.previewScrollTop = scrollTop;

        // Вычисляется отступ для коррекции позиции прокрутки в случае прокрутки к якорю
        // fixme(Золотницкий): Есть небольшой скачек, на 20-30 px
        this.tmpScrollOffset = (vector === "toNext") ? scrollBottom - scrollHeight : scrollTop;

        return {
            next: vector === "toNext" ? scrollBottom >= scrollHeight - UsersScroll.scrollThreshold : false,
            prev: vector === "toPrev" ? scrollTop < UsersScroll.scrollThreshold : false
        };
    };

    /***
     * Обработчик событий scroll и resize, для оптимизации используется throttle
     */
    public refresh = throttle(() => {
        // Если уже загружаются данные, то новая инициализация загрузки может все сломать.
        if (!this.state.isLoadingPrev && !this.state.isLoadingNext && !this.state.isCommonLoading && !this.props.hidden) {
            const isNeedLoading = this.isNeedLoading();

            if (isNeedLoading.next) {
                this.getNext();
            }

            if (isNeedLoading.prev) {
                this.getPreview();
            }
        }
    }, 50);

    // todo(Золотницкий): Пробежаться по коду, можно ли избавиться от дублирования, и нужно ли (тупой код проще)
    /***
     * Обновить все данные в стеке
     */
    public refreshStack = () => {
        const {pagesStack} = this.state;

        if (!pagesStack.length) {
            // Стек пуст, обновлять нечего
            this.getPreview();
        } else {
            const promises = pagesStack.map(pageInfo => axios.get(pageInfo.currentUrl, {cancelToken: this.cancelSource.token}));

            this.setState({...this.state, isCommonLoading: true});
            Promise.all(promises)
                .then(([...responses]) => {

                    const pagesStack: IPageInfo[] = responses.map(response => ({
                        ...response.data,
                        currentUrl: response.config.url,
                        reactItems: response.data.result.map(user => <UserItem key={user.id} {...user} />),
                    }));

                    this.setState({...this.state, pagesStack, isCommonLoading: false});
                })
                .catch(reason => {
                    if (!axios.isCancel(reason)) {
                        console.error(reason);
                        this.setState({...this.state, isCommonLoading: false});
                    }
                })
        }
    };

    protected initRef = (el: HTMLDivElement) => this.containerRef = el;

    render () {
        const {hidden} = this.props;
        return (
            <div
                className={cn({[s.hidden]: hidden})}
                ref={this.initRef}
            >
                <Loading show={this.state.isLoadingNext} />

                {this.state.pagesStack.reduce((items, page) => items.concat(page.reactItems), [])}

                <Loading show={this.state.isLoadingPrev} />
                <FixedLoading show={this.state.isCommonLoading} />
            </div>
        );
    }
}