import * as s from "./User.css";
import * as React from "react";
import * as cn from "classnames";
import axios, {AxiosError} from "axios";
import {IUserScheme} from "../../JsonSchemes/IUserScheme";
import {NotFoundError} from "../404/404";
import {Avatar} from "../Avatar/Avatar";
import {Link} from "react-router-dom";
import {Loading} from "../Loading/Loading";

interface IUserProps {userId: string;}

interface IUserState {
    notFound: boolean;
    user: IUserScheme;
    wait?: boolean;
}

export class User extends React.Component<IUserProps, IUserState> {
    /***
     * Токен для отмены запроса
     */
    protected readonly cancelSource = axios.CancelToken.source();

    constructor(props) {
        super(props);
        this.state = {notFound: false, user: null};
    }

    public componentWillMount() {
        const {userId} = this.props;
        axios.get(`/api/users/${userId}`, {cancelToken: this.cancelSource.token})
            .then(response => this.setState({notFound: false, user: response.data}))
            .catch((error: AxiosError) => {
                if (!axios.isCancel(error)) {
                    if (error.response.status === 404) {
                        this.setState({notFound: true, user: null});
                    } else {
                        console.error(error);
                    }
                }
            });
    }

    public componentWillUnmount() {
        this.cancelSource.cancel();
    }

    private changeNameHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newName = event.currentTarget.value;
        this.setState({...this.state, user: {...this.state.user, name: newName}});
    };

    private formSubmitHandler = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        const newUser = {...this.state.user};
        const userId = newUser.id;
        delete newUser.id;
        this.setWaitStatus(true);
        axios.patch(`/api/users/${userId}`, newUser,{cancelToken: this.cancelSource.token})
            .then(response => this.setWaitStatus(false))
            .catch((error: AxiosError) => {
                if (!axios.isCancel(error)) {
                    console.error(error);
                }
                this.setWaitStatus(false);
            });
    };

    protected setWaitStatus = (wait: boolean) => this.setState({...this.state, wait: wait});

    public render() {
        const {user, notFound, wait} = this.state;
        const isLoading = (user == null && notFound === false || wait);

        let content;
        if (user == null && notFound) {
            content = <NotFoundError key="notFound" />;
        } else if (user) {
            content = (
                <form key="form" className={s.form} onSubmit={this.formSubmitHandler}>
                    <Avatar containerClassName={s.avatar} src={user.avatarUrl} title={`Avatar: ${user.name}`} />
                    <div>
                        <input className={s.name} name="name" value={user.name} onChange={this.changeNameHandler} autoFocus />
                        <button className={cn(s.button, {[s.active]: wait})} type="submit">Ok</button>
                    </div>
                </form>
            );
        }

        return (
            [
                <Loading key="loading" show={isLoading} />,
                <Link key="link" to="/users">To list</Link>,
                content
            ]
        )
    }
}