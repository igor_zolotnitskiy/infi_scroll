import * as React from "react";
import {UsersScroll} from "../UsersScroll/UsersScroll";
import {User} from "../User/User";
import {RouteComponentProps} from "react-router";


interface IUsersProps extends RouteComponentProps<{userId?: string;}>{}

interface IUsersState {}

export class UsersRoute extends React.Component<IUsersProps, IUsersState> {
    public render() {
        const userId = this.props.match.params.userId;

        // Не самое красивое решение, но сделано для того, чтобы вернуться на текущую позицию
        // Можно сделать лучше если хранить в хранилеще № страницы и отступ
        return ([
            <UsersScroll key="list" hidden={userId != null} />,
            userId != null && <User key="item" userId={userId} />
        ]);
    }
}