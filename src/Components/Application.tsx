import * as React from "react";
import {Redirect, Route, Switch} from "react-router";
import {UsersRoute} from "./UsersRoute/UsersRoute";

export const Application = () => {
    return (
        <Switch>
            <Route exact={true} path="/users/:userId?" component={UsersRoute} />
            <Redirect to="/users" />
        </Switch>
    );
};