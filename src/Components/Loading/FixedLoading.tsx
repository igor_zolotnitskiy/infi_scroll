import * as s from "./Loading.css";
import * as React from "react";
import * as cn from "classnames";

interface ILoadingProps {
    show: boolean;
}
export const FixedLoading = ({show}: ILoadingProps) => <div className={cn(s.loading, s.fixed, {[s.show]: show})}></div>;