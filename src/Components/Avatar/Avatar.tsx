import * as s from "./Avatar.css";
import * as React from "react";
import * as cn from "classnames";

interface IAvatarProps {
    src: string;
    containerClassName?: string;
    title?: string;
}

export const Avatar = ({src, containerClassName, title}: IAvatarProps) => (
    <img className={cn(s.avatar, containerClassName)} src={src} title={title} />
);