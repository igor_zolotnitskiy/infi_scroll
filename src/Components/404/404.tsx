import * as React from "react";

export const NotFoundError = () => (
    <h1>
        Ничего не найдено
    </h1>
);