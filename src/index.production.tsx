import "./index.css";

import * as React from "react";
import * as ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { Application } from "./Components/Application";

const rootEl = document.getElementById("root");
const render = (Component: any) => {
    ReactDOM.render(
        <BrowserRouter>
            <Component />
        </BrowserRouter>,
        rootEl,
    );
};

render(Application);