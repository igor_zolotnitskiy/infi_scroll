import axios, {AxiosRequestConfig} from "axios";
const AxiosMockAdapter = require("axios-mock-adapter");
import * as faker from "faker";
import {IUserScheme} from "../JsonSchemes/IUserScheme";

const mock = new AxiosMockAdapter(axios);

/***
 * Получить случайного пользователя
 */
function getRandomUser(): IUserScheme {
    return {
        id: faker.random.uuid(),
        name: faker.name.findName(),
        avatarUrl: faker.image.avatar()
    };
}

/***
 * Получить произвольную задержку ответа api
 */
function getRandomDelay() {
    return faker.random.number({min: 100, max: 700});
}

/***
 * Размер страницы
 */
const pageSize = 30;

/***
 * Кол-во страниц
 */
const maxPages = +(30*100 / pageSize).toFixed();

// Список пользователей
const users = Array(30*100).fill(true).map(getRandomUser);

mock.onGet(/\/api\/users\/p\/\d+/).reply(function(config: AxiosRequestConfig) {
    const pageNum = Number(config.url.split("/").slice(-1)[0]);
    return getPage(pageNum);
});

mock.onGet('/api/users').reply(function(config: AxiosRequestConfig) {
    return getPage(0);
});

function getPage(pageNum: number) {
    return new Promise(function(resolve, reject) {
        setTimeout(function () {
            const response: {
                result?: {id: string; name: string; avatarUrl: string;}[];
                nextPageUrl?: string;
                previousPageUrl?: string;
            } = {};

            if (pageNum < 0 || pageNum > maxPages) {
                return reject([400]);
            }
            if (pageNum >= 0) {
                response.nextPageUrl = `/api/users/p/${pageNum + 1}`;
            }
            if (pageNum <= maxPages && pageNum > 0) {
                response.previousPageUrl = `/api/users/p/${pageNum - 1}`;
            }

            response.result = users.slice(pageNum*pageSize, pageNum * pageSize + pageSize);
            resolve([200, response]);
        }, getRandomDelay());
    });
}

mock.onGet(/\/api\/users\/.+/).reply(function(config: AxiosRequestConfig) {
    const id = config.url.split("/").slice(-1)[0];
    return new Promise(function(resolve, reject) {
        setTimeout(function () {
            const user = users.find(user => user.id === id);
            if (user) {
                resolve([200, user]);
            } else {
                resolve([404]);
            }
        }, getRandomDelay());
    });
});

mock.onPatch(/\/api\/users\/.+/).reply(function(config: AxiosRequestConfig) {
    const id = config.url.split("/").slice(-1)[0];
    return new Promise(function(resolve, reject) {
        setTimeout(function () {
            const user = users.find(user => user.id === id);
            if (user) {
                const newUserData = JSON.parse(config.data);
                user.avatarUrl = newUserData.avatarUrl;
                user.name = newUserData.name;
                resolve([202]);
            } else {
                resolve([404]);
            }
        }, getRandomDelay());
    });
});

