const webpack = require("webpack");
const path = require("path");
const CheckerPlugin = require("awesome-typescript-loader").CheckerPlugin;
const TsConfigPathsPlugin = require("awesome-typescript-loader").TsConfigPathsPlugin;
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".jsx", ".css"]
    },

    devtool: "source-map",

    entry: {
        app: [
            "react-hot-loader/patch",
            path.resolve(__dirname, "src", "index.development.tsx")
        ]
    },

    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "[name].bundle.js",
        chunkFilename: "[id].chunk.js",
        publicPath: "/dist/"
    },

    plugins: [
        new webpack.NamedModulesPlugin(),
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: "'development'"
            }
        }),
        new webpack.HotModuleReplacementPlugin(),
        new CheckerPlugin(),
        new TsConfigPathsPlugin(),
        new HtmlWebpackPlugin({
            title: "Тестовое задание",
            template: "index.ejs"
        })
    ],

    module: {
        rules: [
            {
                test: /\.(tsx|ts|js)$/,
                exclude: path.resolve(__dirname, "node_modules"),
                include: path.resolve(__dirname, "src"),
                use: ["react-hot-loader/webpack", "awesome-typescript-loader"]
            },
            {
                test:   /\.css$/,
                exclude: path.resolve(__dirname, "node_modules"),
                use: [
                    "style-loader",
                    {
                        loader: "css-loader",
                        options: {
                            modules: true,
                            localIdentName: "[name]__[local]___[hash:base64:5]",
                            camelCase: true,
                            importLoaders: 1
                        }
                    },
                    {
                        loader: "postcss-loader",
                        options: {
                            "postcss-import": {},
                            "postcss-cssnext": {
                                warnForDuplicates: false
                            }
                        }
                    }
                ]
            }
        ]
    },

    devServer: {
        publicPath: "/dist/",
        hot: true,
        port: 9000,
        host: "0.0.0.0",
        watchOptions: {
            aggregateTimeout: 300,
            poll: 1000
        },
        historyApiFallback: true,
        proxy: {}
    }
};